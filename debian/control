Source: starjava-vo
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: java
Priority: optional
Build-Depends: ant,
               debhelper-compat (= 13),
               dh-exec,
               javahelper
Build-Depends-Indep: adql-java (>= 2.0~),
                     ant-optional,
                     default-jdk,
                     default-jdk-doc,
                     junit,
                     libandroid-json-org-java,
                     libcommons-io-java,
                     libunity-java,
                     starlink-auth-java,
                     starlink-pal-java,
                     starlink-registry-java,
                     starlink-table-java (>= 4.1.3~),
                     starlink-util-java (>= 1.0+2024.11.05~),
                     starlink-votable-java
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/starjava-vo
Vcs-Git: https://salsa.debian.org/debian-astro-team/starjava-vo.git
Homepage: https://github.com/Starlink/starjava/tree/master/vo
Rules-Requires-Root: no

Package: starlink-vo-java
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends},
         adql-java (>= 2.0~),
         starlink-table-java (>= 4.1.3~),
         starlink-util-java (>= 1.0+2024.11.05~)
Description: Virtual Observatory access classes
 The Virtual Observatory (VO) is the vision that astronomical datasets
 and other resources should work as a seamless whole. This package
 provides Java classes to access VO services within the Starlink context.

Package: starlink-vo-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Recommends: ${java:Recommends}
Description: Virtual Observatory access classes (javadoc)
 The Virtual Observatory (VO) is the vision that astronomical datasets
 and other resources should work as a seamless whole. This package
 provides Java classes to access VO services within the Starlink context.
 .
 This package contains the JavaDoc documentation of the package.
